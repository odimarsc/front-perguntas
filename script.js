//Constantes
//const IpJogo = "10.162.110.165";
const IpJogo = "localhost";
const apiUrl = `http://${IpJogo}:8080`

// Campos iniciais (antes de iniciar o jogo)
const botaoIniciar = document.getElementById("botao-iniciar");
const iniciar = document.getElementById("iniciar");
const nomeJogador = document.getElementById("jogador");

// Campos do jogo
const jogo = document.getElementById("jogo");
const spanQuiz = document.getElementById("quiz");
const tituloQuestao = document.getElementById("titulo");
const categoriaQuestao = document.getElementById("categoria");
const botaoNext = document.getElementById("next");
const placar = document.getElementById("placar");
const totalAcertos = document.getElementById("acertos");
const botaoRestart = document.getElementById("restart");

// resp e radio foram declardads como "class" e portanto serão buscadas como tal
// elementos da mesma "class" são iteráveis
const resps = document.querySelectorAll(".resp");
const radios = document.querySelectorAll(".radio");


// cria o "class" dadosJogos para receber o json dos gets
let dadosJogo = {
    id: this.id,
    nomeUsuario: this.nomeUsuario,
    qtdePerguntas: this.qtdePerguntas,
    qtdeAcertos: this.qtdeAcertos,
    listaPerguntas: this.listaPerguntas
}

// **********************************************************************************
//            Funções auxiliares de controle dos eleementos de tela 
// **********************************************************************************
function esconde(obj) {
    obj.style.display = "none";
}

function mostra(obj) {
    obj.style.display = "block";
}

function bloqueia (obj) {
    jogo.style.display = "block";
}

function limpaRadios() 
{
    for( var i=0; i<radios.length; i++ )
	{
        radios[i].checked = false;
 	}
}

// Controle integrado dos radios buttons => quando "checar" um "descheca" os demais
function changeRadios()
{  
    for(let radio of radios){
     if (this.name !== radio.name){
            radio.checked = false;
        }
    }
}

// Retorna o número da resposta selecionada (1 a N). Se não houve seleção retorna 0 (zero)
function pegaRespostaRadio() {
    let i=0;
    for(let radio of radios) {
       i++;
       if( radio.checked )
           return i;
   }
   return 0;
}

// **********************************************************************************
//               Funcoes do controle do Jogo
// **********************************************************************************

function getStart() {

        fetch(`${apiUrl}/game/start/${nomeJogador.value}`)
        .then(promessa => promessa.json())
        .then(dados => {
                inicializaJogo(dados);
            }
        );
}

function inicializaJogo(dados_entrada) {
    dadosJogo.id = dados_entrada.id;
    dadosJogo.nomeUsuario = dados_entrada.nomeUsuario;
    dadosJogo.qtdePerguntas = dados_entrada.qtdePerguntas;
    dadosJogo.qtdeAcertos = dados_entrada.qtdeAcertos;
    dadosJogo.listaPerguntas = dados_entrada.listaPerguntas;

    if (dadosJogo !== null) { 
        montaPerguntas(dadosJogo);
    }
}

function montaPerguntas(dados) {
    
    // se fim de jogo mostra placar e retorna
    if (dados.qtdePerguntas == 0) {

        console.log("Fim de Jogo!");
        esconde(jogo);
        totalAcertos.innerHTML = `<Fim> Total de acertos: ${dados.qtdeAcertos}`;
        mostra(placar);
        //post (`${apiUrl}/ranking`, dadosJogo.qtdePerguntas[0]);
        console.log(`Dados finais:`);
        console.log(dados);
        return;
    }

    dadosJogo.id = dados.id;
    dadosJogo.nomeUsuario = dados.nomeUsuario;
    dadosJogo.qtdePerguntas = dados.qtdePerguntas;
    dadosJogo.qtdeAcertos = dados.qtdeAcertos;
    dadosJogo.listaPerguntas = dados.listaPerguntas;

    if (dadosJogo.listaPerguntas) {
        // Pega somente a primeira pergunta da lista de perguntas (indice=0)
        tituloQuestao.innerHTML = `Question ${dadosJogo.listaPerguntas[0].id}: ${dadosJogo.listaPerguntas[0].title}`;
        categoriaQuestao.innerHTML = dadosJogo.listaPerguntas[0].category;
        i=0;
        resps[i++].innerHTML = dadosJogo.listaPerguntas[0].option1;
        resps[i++].innerHTML = dadosJogo.listaPerguntas[0].option2;
        resps[i++].innerHTML = dadosJogo.listaPerguntas[0].option3; 
        resps[i].innerHTML = dadosJogo.listaPerguntas[0].option4; 
    }
}


function getPerguntasById(id) {
    fetch(`${apiUrl}/game/start/perguntas/${dadosJogo.id}`)
    .then(promessa => promessa.json())
    .then(dados => {
            montaPerguntas(dados);
          }
    );
}

function post(endpoint, data) {
    return fetch(apiUrl + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'content-type': 'application/json'
        }
    }).then(promessa => promessa.json())
      .then((response) => {
            
            return response.json;
            }
        );
}

function postBruno(endpoint, data){
    return fetch(apiUrl + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    });
}

function iniciaJogo() {

    esconde(iniciar);
    esconde(placar);

    // carrega inicio do jogo
    getStart();

    limpaRadios();

    mostra(jogo);
}


function enviaResposta () {
    post(`/game/start/respostas/${dadosJogo.id}`, dadosJogo);
}


function nextQuestion () 
{
    let numResp = pegaRespostaRadio();
    console.log(`numResp: ${numResp}`);
    
    // se houve selecao da resposta, envia a resposta e vai para proxima pergunta 
    if ( numResp > 0 ) { 
        dadosJogo.listaPerguntas[0].respostaUsuario = numResp;
        
        enviaResposta();
        
        getPerguntasById(dadosJogo.id);
            
        limpaRadios();
    }
    else {
        alert("Favor selecionar uma resposta!");
        spanQuiz.focus();
    }
}

// **********************************************************************************
//               Atribuiçao dos eventos
// **********************************************************************************
botaoIniciar.onclick = iniciaJogo;

for(let radio of radios){
    radio.onchange = changeRadios;
}

botaoNext.onclick = nextQuestion;

botaoRestart.onclick = iniciaJogo;